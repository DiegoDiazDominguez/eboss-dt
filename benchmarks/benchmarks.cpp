//
// Created by diediaz on 27-11-18.
//

#include <iostream>
#include <fstream>
#include <r_boss.hpp>

int main(int argc, char* argv[]) {

    double t_outd=0,t_for=0,t_ind=0, t_rev_comp=0, t_fovps=0, t_back=0, t_build_l=0, t_ncontained=0, t_label=0;
    clock_t begin, end;
    size_t n_samp=0;

    if(argc==1){
        std::cout<<"missing dbg index"<<std::endl;
        exit(1);
    }


    r_boss boss;
    std::string input_file(argv[1]);
    sdsl::load_from_file(boss, input_file);

    /*
    //TODO testing
    for(size_t i=1;i<10;i++){
        size_t solid = boss.dollar_pref_ss(i);
        boss.foverlaps_v2(solid);
    }
    //*/

    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<size_t> distr(1, boss.n_solid_nodes); // define the range
    std::vector<size_t> trans_nodes(boss.k);
    std::vector<uint8_t> label(boss.k);
    r_boss::degree_t ind;

    for(size_t i=0;i<1000;i++){

        size_t solid_node = boss.solid_nodes_ss(distr(eng));
        std::pair<size_t, bool> outd = boss.dbg_outdegree(solid_node);

        if((outd.first-outd.second)==0) continue;

        begin = clock();
        boss.dbg_outdegree(solid_node);
        end = clock();
        t_outd+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.dbg_indegree(solid_node);
        end = clock();
        t_ind+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.dbg_outgoing(solid_node, 1);
        end = clock();
        t_for+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        ind = boss.dbg_indegree(solid_node);
        size_t number = (rand() % ind.first)+1;
        begin = clock();
        boss.dbg_incomming(solid_node, uint8_t (number));
        end = clock();
        t_back+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.reverse_complement(solid_node);
        end = clock();
        t_rev_comp+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.next_contained(solid_node);
        end = clock();
        t_ncontained+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.build_l(solid_node);
        end = clock();
        t_build_l+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.f_overlaps(solid_node);
        end = clock();
        t_fovps+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.node_label(solid_node);
        end = clock();
        t_label+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        n_samp++;
    }

    std::cout<<"#File_name\t"
               "Outdegree(usecs)\t"
               "Indegree(usecs)\t"
               "Forward(usecs)\t"
               "Backward(usecs)\t"
               "Foverlaps(usecs)\t"
               "Rev_comp(usecs)\t"
               "Next_contained(usecs)\t"
               "BuildL(usecs)\t"
               "Label(usecs)"
               <<std::endl;

    std::cout<<input_file<<"\t"
             <<t_outd/n_samp<<"\t"
             <<t_ind/n_samp<<"\t"
             <<t_for/n_samp<<"\t"
             <<t_back/n_samp<<"\t"
             <<t_fovps/n_samp<<"\t"
             <<t_rev_comp/n_samp<<"\t"
             <<t_ncontained/n_samp<<"\t"
             <<t_build_l/n_samp<<"\t"
             <<t_label/n_samp<<"\t"<<std::endl;

    return 0;
}

