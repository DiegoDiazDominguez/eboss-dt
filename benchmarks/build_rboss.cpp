//
// Created by Diego Diaz on 2019-01-17.
//
#include <r_boss.hpp>

int main(int argc, char* argv[]) {
    if(argc<4){
        std::cout<<"usage: program input_fq K m output_prefix"<<std::endl;
        exit(1);
    }

    cache_config config(false, ".", "tmp");
    auto k = size_t(strtol(argv[2], &argv[2], 10));
    auto m = size_t(strtol(argv[3], &argv[2], 10));
    std::string input_fastq(argv[1]);
    std::string output_prefix(argv[4]);
    std::cout<<"Building the rBOSS index"<<std::endl;
    r_boss rb_index(input_fastq, config, k, m);

    struct rusage r_usage{};
    getrusage(RUSAGE_SELF, &r_usage);
    double memory_peak = 0;

#ifdef __APPLE__
    memory_peak = r_usage.ru_maxrss/(1024*1024);
#elif __linux__
    memory_peak = r_usage.ru_maxrss/(1024.0);
#endif
    std::ofstream os;
    double index_size = size_in_mega_bytes(rb_index);
    os.open(output_prefix+"_dbg_paper_stats");
    os<<"##min K: "+std::to_string(rb_index.m)+"\n";
    os<<"##max K: "+std::to_string(rb_index.k)+"\n";
    os<<"##tot nodes: "+std::to_string(rb_index.num_of_nodes())+"\n";
    os<<"##tot solid nodes: "+std::to_string(rb_index.n_solid_nodes)+"\n";
    os<<"##tot pref nodes: "+std::to_string(rb_index.num_of_nodes()-rb_index.n_solid_nodes)+"\n";
    os<<"##tot edges: "+std::to_string(rb_index.num_of_edges())+"\n";
    os<<"##dollar edges: "+std::to_string(rb_index.num_of_dollar_edges())+"\n";
    os<<"##marked edges: "+std::to_string(rb_index.num_of_marked_edges())+"\n";
    os<<"##ovp tree nodes: "+std::to_string(rb_index.ovp_tree.get_tot_nodes())+"\n";
    os<<"##ovp int tree nodes: "+std::to_string(rb_index.ovp_tree.get_int_nodes())+"\n";
    os<<"##Index size: "+std::to_string(sdsl::size_in_mega_bytes(rb_index))+" MB"+"\n";
    os<<"###Run-length EdgeBWT: "+std::to_string((size_in_mega_bytes(rb_index.edge_bwt)/index_size)*100)+"%\n";
    os<<"###Overlap tree: "+std::to_string((size_in_mega_bytes(rb_index.ovp_tree)/index_size)*100)+"%\n";
    os<<"###Node marks: "+std::to_string((size_in_mega_bytes(rb_index.node_marks)/index_size)*100)+"%\n";
    os<<"###Solid nodes marks: "+std::to_string((size_in_mega_bytes(rb_index.solid_nodes)/index_size)*100)+"%\n";
    os<<"##input_file: "+output_prefix+"\n";
    os<<"##memory_peak: "+std::to_string(memory_peak)+" MB\n";
    os<<"##elapsed_time: "+std::to_string(r_usage.ru_utime.tv_sec)+" seconds\n";
    os.close();
    std::cout<<"Storing index to file "<<output_prefix+".boss"<<std::endl;
    store_to_file(rb_index, output_prefix+".boss");
    std::cout<<"Done !"<<std::endl;
}
