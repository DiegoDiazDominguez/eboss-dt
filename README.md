#rBOSS library

This C++ library is an implementation of the rBOSS data structure described [here](https://arxiv.org/abs/1901.10453)


##Requisites
1. SDSL-lite library
2. Cmake >=3.7

##Installation
```
$ git clone https://DiegoDiazDominguez@bitbucket.org/DiegoDiazDominguez/eboss-dt.git
$ cd eboss-dt 
$ mkdir build && cd build
$ cmake -DUSER_INCLUDE=/path/to/SDSL/include/dir -DLIB_INCLUDE=/path/to/SDSL/lib/dir ..
$ make
$ make DESTDIR=/path/to/install/dir install
``` 

##Minimal working example
example.cpp
```cpp
#include <iostream>
#include <fstream>
#include <r_boss.hpp>

int main(int argc, char* argv[]) {
    std::string input_fastq="test_file.fq";
    size_t k=50;
    size_t m=15;
    cache_config config(false, ".", "tmp");
    r_boss rb_index(input_fastq, config, k, m);
}
```

##Benchmarks

Assuming you have an input fastq file **my_fastq.fq**, you can test rBOSS primitives as follows: 

```
cd bin
./b_rboss my_fastq.fq max_k min_m output_prefix
./rboss_bench output_prefix.boss
```

Where max_k is the maximum dBG order allowed, min_m is the minimum overlap between dBG nodes.

##VO-BOSS

In our paper, we compared rBOSS with VO-BOSS. You can find our VO-BOSS implementation [here](https://bitbucket.org/DiegoDiazDominguez/voboss/src/master/)

##Compilation
```
$g++ example.cpp -I/path/sdsl/include -L/path/sdsl/lib -I/path/rboss/headers -L/path/rboss/lib -o rboss_example -lrboss -lsdsl -ldivsufsort -ldivsufsort64 -lz
```

##Assembler implementation
The assembler can be found [here](https://bitbucket.org/DiegoDiazDominguez/eboss-assembler/src/master/)
