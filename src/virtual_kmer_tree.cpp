//
// Created by Diego Diaz on 4/5/18.
//

#include "virtual_kmer_tree.hpp"

using namespace sdsl;
using namespace std;

virtual_kmer_tree::virtual_kmer_tree(sdsl::cache_config& config, size_t K):
        sa(cache_file_name(conf::KEY_SA, config)),
        lcp(cache_file_name("klcp", config)),
        up(cache_file_name("up_vst", config), std::ios::out, 1000000),
        down(cache_file_name("down_vst", config), std::ios::out, 1000000),
        next_l_index(cache_file_name("next_l_index_vst", config), std::ios::out, 1000000){
    m_K = K;
    std::stack<int64_t> new_stack;
    new_stack.push(0);
    long long int last_index = -1;
    size_type lcp_value;

    for(size_type j=0;j<=lcp.size();j++) next_l_index[j] = 0;
    for(size_type j=0;j<=lcp.size();j++) up[j] = 0;
    for(size_type j=0;j<=lcp.size();j++) down[j] = 0;

    // build down and up arrays
    for(size_type i=0; i<=lcp.size();i++){

        lcp_value = (i == lcp.size()) ? size_type(0) : lcp[i];

        while(lcp_value < lcp[new_stack.top()]){
            last_index = new_stack.top();
            new_stack.pop();
            if(lcp_value <= lcp[new_stack.top()] && lcp[new_stack.top()] != lcp[last_index]){
                down[new_stack.top()] = last_index;
            }
        }

        if(last_index != -1){
            up[i] = last_index;
            last_index = -1;
        }
        new_stack.push(i);
    }

    //build next l index array
    new_stack = std::stack<int64_t>();
    new_stack.push(0);
    for(size_type i=0;i<lcp.size();i++){
        while(lcp[i]<lcp[new_stack.top()]){
            new_stack.pop();
        }
        if(lcp[i] == lcp[new_stack.top()]){
            last_index = new_stack.top();
            new_stack.pop();
            next_l_index[last_index] = i;
        }
        new_stack.push(i);
    }


    // this is to handle the last leaf
    //up[up.size()-1] = up.size()-2;

    //a bit vector with the positions of the '$' symbols
    bit_vector_type dollar_bv;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    rank_1_type dollar_rank(&dollar_bv);
    m_dollar_bv.swap(dollar_bv);
    m_rank_1_supp.swap(dollar_rank);
    m_rank_1_supp.set_vector(&m_dollar_bv);

}

vector<virtual_kmer_tree::node_type>
virtual_kmer_tree::get_children(virtual_kmer_tree::node_type node) {

    size_type i,j, i_1,i_2, depth;

    i = get<0>(node);
    j = get<1>(node);

    if(i==j || std::get<2>(node)==m_K){
       return std::vector<node_type>();
    }

    std::vector<node_type> children;

    if(!is_root(node)) {

        if (up[j + 1] <= j && i < up[j + 1]) {
            i_1 = up[j + 1];
        } else {
            i_1 = down[i];
        }

        depth = get_depth(i, i_1-1);
        children.push_back({i, i_1-1, depth});
    }else{
        i_1 = 0;
    }

    while(next_l_index[i_1] != 0){
        i_2 = next_l_index[i_1];

        depth = get_depth(i_1, i_2-1);
        children.push_back({i_1, i_2-1, depth});
        i_1 = i_2;
    }

    depth = get_depth(i_1, j);
    children.push_back({i_1, j, depth});

    return children;
}

virtual_kmer_tree::size_type
virtual_kmer_tree::get_depth(virtual_kmer_tree::size_type lb, virtual_kmer_tree::size_type rb) {

    if(lb!=rb){ //internal node
        if(lb < up[rb+1] && up[rb+1] <= rb){
            return lcp[up[rb+1]];
        }else{
            return lcp[down[lb]];
        }
    }else{ //leaf
        return m_K;//sa.size() - sa[lb];
    }
}

virtual_kmer_tree::size_type virtual_kmer_tree::n_leaves() {
    return lcp.size();
}

bool virtual_kmer_tree::is_dollar_valid(virtual_kmer_tree::node_type node, virtual_kmer_tree::size_type K) {
    size_type lb, rb, n_dollars, tmp_rank;

    lb = sa[get<0>(node)];

    if((lb+K)>=sa.size()){
       return false;
    }

    rb = lb+K;
    tmp_rank = m_rank_1_supp(lb);
    n_dollars = (m_rank_1_supp(rb) - tmp_rank);

    if(n_dollars>0){ //check the dollars are a suffix of the kmer
        n_dollars = m_rank_1_supp(rb-n_dollars) - tmp_rank;
        return (n_dollars==0);
    }else{
       return true;
    }
}

bool virtual_kmer_tree::is_root(virtual_kmer_tree::node_type node) {
    return (get<0>(node) ==0 && get<1>(node)==(n_leaves()-1));
}

virtual_kmer_tree::~virtual_kmer_tree() {
    up.close(true);
    down.close(true);
    next_l_index.close(true);
}

bool virtual_kmer_tree::ends_with_dollar(virtual_kmer_tree::node_type node) {
    size_type lb, rb;
    lb = sa[get<0>(node)];
    rb = lb + get<2>(node);


    return (m_rank_1_supp(rb) - m_rank_1_supp(rb-1)) > 0;
}
