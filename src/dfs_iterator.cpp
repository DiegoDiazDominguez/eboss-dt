//
// Created by Diego Diaz on 3/25/18.
//

#include "dfs_iterator.hpp"

using namespace sdsl;

DfsIterator::DfsIterator(const bit_vector *shape, DfsIterator::value_type starter, DfsIterator::value_type final,
                         const DfsIterator::t_bp_support *bp_supp): curr_rank(m_curr_rank),
                                                                    m_shape(shape),
                                                                    m_bp_supp(bp_supp)

{
    if(!(*m_shape)[(*m_bp_supp).select(starter)]){
        throw std::invalid_argument("no valid ID to start DFS");
    }
    m_curr_rank = starter;
    m_final_rank = final;
}

DfsIterator::value_type DfsIterator::operator*() const {
    return (*m_bp_supp).select(m_curr_rank);
}

DfsIterator &DfsIterator::operator++() {
    m_curr_rank++;
    if(m_curr_rank>m_final_rank){ //if we complete the DFS we go to the root
       m_curr_rank = 1;
    }
    return *this;
}

const DfsIterator DfsIterator::operator++(int) {
    DfsIterator tmp = *this;
    ++m_curr_rank;
    if(m_curr_rank>m_final_rank){ //if we complete the DFS we go to the root
        m_curr_rank = 1;
    }
    return tmp;
}

DfsIterator &DfsIterator::operator--() {
    if(m_curr_rank>0){ // to avoid negative positions
        --m_curr_rank;
    }else {
        m_curr_rank = 1;
    }
    return *this;
}

const DfsIterator DfsIterator::operator--(int) {
    DfsIterator tmp = *this;
    if(m_curr_rank>0){ // to avoid negative positions
        --m_curr_rank;
    }else {
        m_curr_rank = 1;
    }
    return tmp;
}

bool DfsIterator::operator==(const DfsIterator &rhs) {
    return (m_curr_rank == rhs.m_curr_rank);
}

bool DfsIterator::operator!=(const DfsIterator &rhs) {
    return (m_curr_rank != rhs.m_curr_rank);
}

