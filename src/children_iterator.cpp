//
// Created by diediaz on 13-12-18.
//

#include <children_iterator.hpp>
#include "children_iterator.hpp"

children_iterator::size_type children_iterator::operator*() const {
    return m_curr_child;
}

bool children_iterator::hasChildren() {
    return m_has_children;
}

const children_iterator children_iterator::operator--(int) {

    if(m_curr_child!=0) {
        m_curr_child--;
        if ((*m_tree_shape)[m_curr_child]) {
            m_curr_child = 0;
        }else{
            m_curr_child=m_bp_supp->find_open(m_curr_child);
        }
    }
    return *this;
}

children_iterator &children_iterator::operator=(const children_iterator& other) {


    m_curr_child = other.m_curr_child;
    m_tree_shape = other.m_tree_shape;
    m_bp_supp = other.m_bp_supp;
    m_has_children = other.m_has_children;
    return *this;
}

children_iterator::children_iterator(const sdsl::bit_vector *tree_shape, const children_iterator::t_bp_support *bp_supp,
                                   children_iterator::size_type father_node): m_tree_shape(tree_shape),
                                                                             m_bp_supp(bp_supp){

    assert(father_node<tree_shape->size() && (*tree_shape)[father_node]);
    m_curr_child = bp_supp->find_close(father_node)-1;

    if((*m_tree_shape)[m_curr_child]){
        m_curr_child = 0;
        m_has_children = false;
    }else{
        m_curr_child = bp_supp->find_open(m_curr_child);
        m_has_children = true;
    }
}

children_iterator::children_iterator() = default;
