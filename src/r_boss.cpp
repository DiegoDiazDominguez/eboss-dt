//
// Created by Diego Diaz on 3/19/18.
//

#include <r_boss.hpp>

using namespace std;
using namespace sdsl;

r_boss::r_boss(string &input_file, cache_config &config, const size_t & K, const size_t & min_str_depth):
        m_k(K), m_m(min_str_depth){

    FastXParser::preproc_reads(input_file, config, 1);
    build_sa_bwt_lcp(config);
    build_klcp(config, K-1);
    build_edgebwt(config, K-1);
    build_overlap_tree(config, K-1, min_str_depth);

    //build the Run-length edgeBWT
    rle_ebwt ebwt(config);
    m_edge_bwt.swap(ebwt);

    load_from_file(m_node_marks, cache_file_name("node_marks", config));
    m_node_marks_ss.set_vector(&m_node_marks);
    m_node_marks_rs.set_vector(&m_node_marks);

    load_from_file(m_solid_nodes, cache_file_name("solid_nodes", config));
    m_solid_nodes_ss.set_vector(&m_solid_nodes);

    /*for(size_t i=0;i<m_ovp_tree.n_leaves;i++) {
        std::vector<uint8_t> tmp = node_label(i);
        for(size_t j=0;j<m_k-1;j++){
           std::cout<<dna_alphabet::comp2char[tmp[j]];
        }
        std::cout<<""<<std::endl;
    }*/

    /*std::vector<uint8_t> symbols(7,0);
    std::vector<uint64_t> rank_i(7,0);
    std::vector<uint64_t> rank_j(7,0);

    std::string test;
    test.resize(4);
    test[0] = 5;
    test[1] = 2;
    test[2] = 5;
    test[3] = 2;
    backward_search(test, 1);*/

    //this is for testing has_one_symbol
    /*uint8_t tmp_symbol;
    size_t rank_i, rank_j;
    int_vector_buffer<8> ebwt_test(cache_file_name("ebwt", config));
    sdsl::bit_vector bv;
    bv.resize(ebwt_test.size());
    bv[0] = true;
    uint8_t last = ebwt[0];
    for(size_t i=1;i<bv.size();i++){
        if(ebwt[i]==1){
            bv[i] = false;
        }else {
            bv[i] = ebwt[i] != last;
            last = ebwt[i];
        }
    }

    for(size_t i=0;i<bv.size();i++){
        std::cout<<i<<" "<<bv[i]<<std::endl;
    }

    size_t acc=0;
    for(size_t i=0;i<bv.size()-1;i++){
        for(size_t j=i;j<bv.size();j++){
            if(bv[i]){
                acc=0;
            }else{
                acc=1;
            }

            clock_t begin = clock();
            bool tmp = ebwt.has_one_symbol(i,j,tmp_symbol, rank_i, rank_j);
            //clock_t end = clock();
            //double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
            //std::cout<<std::fixed<<elapsed_secs<<std::endl;

            for(size_t k=i;k<=j;k++){
                if(bv[k]){
                   acc++;
                }
            }
            if(ebwt[i]!=1 && ((acc>1 && tmp) || (acc==1 && !tmp))){
                std::cout<<i<<" "<<j<<" "<<acc<<" "<<tmp<<std::endl;
                std::cout<<"tenemos problemas"<<std::endl;
            }
        }
    }*/
    //this is for testing edge_rank
    /*for(size_t i=0;i<ebwt.size();i++){
        for(uint8_t j=1;j<6;j++){
            if(ebwt.rank(i,j) != freqs_table[i][j]) {
                std::cout <<i<<" "<<dna_alphabet::comp2char[j]<<" "<<ebwt.edge_rank(i, j) << " " << freqs_table[i][j] << std::endl;
            }
        }
    }
    std::cout<<ebwt.size_in_MB()<<std::endl;
    //this is for testing access
    int_vector_buffer<8> ebwt_test(cache_file_name("ebwt", config));
    for(size_t i=0;i<ebwt_test.size();i++){
        if(ebwt_test[i]!=ebwt[i]){
            std::cout<<i<<" "<<ebwt_test[i]<<" "<<int(ebwt[i])<<std::endl;
            exit(1);
        }
    }*/

    //delete temporal files
    sdsl::util::delete_all_files(config.file_map);
    /*double index_size = size_in_mega_bytes(*this);
    LOG(INFO)<<"BOSS index finished";
    LOG(INFO)<<"Stats:";
    LOG(INFO)<<" Minimum Kmer size: "+to_string(m);
    LOG(INFO)<<" Maximum Kmer size: "+to_string(k);
    LOG(INFO)<<" Number of nodes: "+to_string(num_of_nodes());
    LOG(INFO)<<" Number of edges: "+to_string(num_of_edges());
    LOG(INFO)<<" The size of the index: "+to_string(index_size)+" MB";
    LOG(INFO)<<"  Run-length EdgeBWT: "+to_string((size_in_mega_bytes(m_edge_bwt)/index_size)*100)+"%";
    LOG(INFO)<<"  Overlap tree: "+to_string((size_in_mega_bytes(m_ovp_tree)/index_size)*100)+"%";
    LOG(INFO)<<"  Node marks: "+to_string((size_in_mega_bytes(m_node_marks)/index_size)*100)+"%";
    LOG(INFO)<<"  Solid marks: "+to_string((size_in_mega_bytes(m_solid_nodes)/index_size)*100)+"%";*/
}

void r_boss::build_sa_bwt_lcp(cache_config &config){
    // construct SA
    //LOG(INFO)<<"Building the SA array";
    construct_sa<8>(config);
    register_cache_file(conf::KEY_SA, config);

    // construct BWT
    //LOG(INFO)<<"Building the BWT array";
    construct_bwt<8>(config);
    register_cache_file(conf::KEY_BWT, config);

    // construct LCP
    //LOG(INFO)<<"Building the LCP array";
    construct_lcp_semi_extern_PHI(config);
    register_cache_file(conf::KEY_LCP, config);
}

r_boss::size_type r_boss::serialize(std::ostream &out, structure_tree_node *v, string name) const{

    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_t written_bytes = 0;

    written_bytes += m_edge_bwt.serialize(out, child, "edge_bwt");
    written_bytes += m_ovp_tree.serialize(out, child, "tree_shape");
    written_bytes += m_node_marks.serialize(out, child, "node_marks");
    written_bytes += m_node_marks_rs.serialize(out, child, "node_marks_rs");
    written_bytes += m_node_marks_ss.serialize(out, child, "node_marks_ss");
    written_bytes += m_solid_nodes.serialize(out, child, "m_solid_nodes");
    written_bytes += m_solid_nodes_ss.serialize(out, child, "m_solid_nodes_ss");
    written_bytes += write_member(m_k, out, child, "k");
    written_bytes += write_member(m_m, out, child, "m");
    written_bytes += write_member(m_n_solid_nodes, out, child, "n_solid_nodes");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void r_boss::load(std::istream& in){
    m_edge_bwt.load(in);
    m_ovp_tree.load(in);
    m_node_marks.load(in);
    m_node_marks_rs.load(in, &m_node_marks);
    m_node_marks_ss.load(in, &m_node_marks);
    m_solid_nodes.load(in);
    m_solid_nodes_ss.load(in, &m_solid_nodes);
    read_member(m_k, in);
    read_member(m_m, in);
    read_member(m_n_solid_nodes, in);
}

r_boss::r_boss(): node_marks(m_node_marks),
                        edge_bwt(m_edge_bwt),
                        ovp_tree(m_ovp_tree),
                        node_marks_rs(m_node_marks_rs){}


void r_boss::build_klcp(cache_config &config, size_t K) {

    //LOG(INFO)<<"Building the LCP of the kmers";

    size_t n_dollars;
    int_vector_buffer<> lcp(cache_file_name(conf::KEY_LCP, config));
    int_vector_buffer<> klcp(cache_file_name("klcp", config), std::ios::out, 1000000);

    int_vector_buffer<> sa(cache_file_name(conf::KEY_SA, config));
    sdsl::sd_vector<> dollar_bv;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    sdsl::sd_vector<>::select_1_type ds(&dollar_bv);
    n_dollars = dr.rank(dollar_bv.size());

    klcp[0] = 0;
    //dummy node
    for(size_t i=1;i<=n_dollars;i++){
        klcp[i] = K;
    }

    for(size_t i=(n_dollars+1);i<lcp.size();i++){
        //modify the LCP
        if(lcp[i]>K || lcp[i]>=(ds.select(dr.rank(sa[i]+1)+1)-sa[i]+1)){
            klcp[i]=K;
        }else{
            klcp[i] = lcp[i];
        }
    }

    lcp.close();
    klcp.close();
    register_cache_file("klcp", config);
}

void r_boss::build_edgebwt(cache_config &config, size_t K) {

    //LOG(INFO)<<"Building the edgeBWT";
    bool flag_symbols;
    std::bitset<6> kmer_symbols;
    std::bitset<6> flagged_symbols=false;
    bit_vector in_marks, node_marks;
    int_vector_buffer<8> bwt(cache_file_name(conf::KEY_BWT, config));
    int_vector_buffer<8> ebwt(cache_file_name("ebwt", config), std::ios::out);
    int_vector_buffer<> klcp(cache_file_name("klcp", config));
    size_t bwt_pos=0;

    //this is for marking dollar-prefixed kmers
    sdsl::sd_vector<> dollar_bv;
    bit_vector dollar_marks;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    int_vector_buffer<> sa(cache_file_name(conf::KEY_SA, config));
    util::assign(dollar_marks, int_vector<1>(bwt.size(), 0));
    size_t dpos=0;
    //

    //TODO just testing
    /*
    int_vector_buffer<8> text(cache_file_name(conf::KEY_TEXT, config));
    size_t kpos=0, epos=0;
    bool is_dollar_prefixed;
     */
    //

    util::assign(node_marks, int_vector<1>(bwt.size(), 1));
    util::assign(in_marks , int_vector<1>(bwt.size(), 0));

    //TODO just testing
    /*
    for(size_t i=0;i<text.size();i++){
        std::cout<<i<<" "<<klcp[i]<<" ";
        for(size_t j=sa[i];j<text.size();j++){
            std::cout<<dna_alphabet::comp2char[text[j]];
        }
        std::cout<<""<<std::endl;
    }*/
    //

    for(size_t i=1;i<=klcp.size();i++){

        //TODO testing
        //bool first=true;

        if(i==klcp.size() || klcp[i]<K){

            //this is for marking kmers starting with dollars
            if(dr.rank(min(sa[i-1]+K, sa.size()))-dr.rank(sa[i-1])==0){
                dollar_marks[dpos]=true;
                m_n_solid_nodes++;
            }
            //

            //store the symbols of the previous kmer
            for(size_t j=1;j<kmer_symbols.size();j++){
                if(kmer_symbols[j]){

                    if(flagged_symbols[j] && j>1) {// edge is marked (except $ symbols)
                        in_marks[bwt_pos] = true;
                    }


                    //TODO just checking
                    /*
                    if(in_marks[bwt_pos]){
                       std::cout<<"-";
                    }
                    std::cout<<epos<<"\t"<<dna_alphabet::comp2char[j]<<"\t";
                    epos++;
                    if(first) {
                        bool dollar_start = false;
                        for (size_t k = 0; k < K; k++) {
                            if ( (sa[i-1]+k)>=text.size() || (text[sa[i - 1] + k]) == 1) {
                                dollar_start = true;
                            }
                            if (!dollar_start) {
                                std::cout << dna_alphabet::comp2char[text[sa[i - 1] + k]];
                            } else {
                                std::cout << "$";
                            }
                        }
                        std::cout<<"\t"<<is_dollar_prefixed<<"\t"<<kpos;
                        kpos++;
                    }
                    first=false;

                    std::cout<<""<<std::endl;
                     */
                    //

                    ebwt[bwt_pos]=j;
                    bwt_pos++;
                }
            }
            node_marks[bwt_pos-1] = false;
            dpos++;
            if(i==klcp.size()){
               break;
            }

            //setup for the next Kmer;
            flag_symbols = klcp[i] == (K - 1);
            if (!flag_symbols) {
                flagged_symbols.reset();
            } else {
                flagged_symbols |= kmer_symbols;
            }
            kmer_symbols.reset();
        }
        kmer_symbols.set(bwt[i]);
    }

    node_marks.resize(bwt_pos);
    in_marks.resize(bwt_pos);

    ebwt.close();
    klcp.close();

    //TODO just testing
    //sdsl::wt_huff<rrr_vector<63>> wt;
    //construct(wt, cache_file_name("ebwt", config));
    //std::cout<<size_in_mega_bytes(wt)<<std::endl;

    //this is for marking dollar-prefixed kmers
    dollar_marks.resize(dpos);
    sdsl::rrr_vector<63> rrr_dollar_marks(dollar_marks);
    store_to_file(rrr_dollar_marks, cache_file_name("solid_nodes", config));
    //

    sdsl::rrr_vector<63> tmp_node_marks(node_marks);
    store_to_file(tmp_node_marks, cache_file_name("node_marks", config));
    store_to_file(in_marks, cache_file_name("in_marks", config));

    register_cache_file("solid_nodes", config);
    register_cache_file("node_marks", config);
    register_cache_file("in_marks", config);
    register_cache_file("ebwt", config);
}

std::vector<r_boss::range_t> r_boss::backward_search(vector<uint8_t> &query, uint8_t k) {

    std::stack<std::tuple<size_t, size_t, uint8_t, size_t>> ranges;
    std::vector<std::pair<size_t, size_t>> res;

    std::vector<uint8_t> symbols(7,0);
    std::vector<uint64_t> rank_i(7,0);
    std::vector<uint64_t> rank_j(7,0);
    uint8_t mm;
    size_t e_start, e_end, k_start, k_end, last_index, n_symbols;

    std::tuple<size_t, size_t, uint8_t, size_t> root(0,m_edge_bwt.m_runs_acc[5], 0, query.size());
    ranges.push(root);

    while(!ranges.empty()){
        std::tuple<size_t, size_t, uint8_t, uint8_t> tmp = ranges.top();
        ranges.pop();
        last_index = std::get<3>(tmp);
        if(last_index>0) {

            if(std::get<0>(tmp)==0){
                e_start=0;
            }else {
                e_start = m_node_marks_ss.select(std::get<0>(tmp)) + 1;
            }
            e_end=m_node_marks_ss.select(std::get<1>(tmp)+1);

            n_symbols = m_edge_bwt.range_symbols(e_start,
                                                 e_end,
                                                 symbols, rank_i, rank_j);
            for(size_t i=0;i<n_symbols;i++){
                mm = std::get<2>(tmp) + (symbols[i] != query[last_index - 1]);
                if (mm <= k) {

                    k_start = m_edge_bwt.runs_acc[symbols[i]-1] + rank_i[i];
                    k_end = m_edge_bwt.runs_acc[symbols[i]-1] + rank_j[i];

                    std::tuple<size_t, size_t, uint8_t, size_t> next_range(k_start, k_end, mm, last_index-1);
                    ranges.push(next_range);
                }
            }
        }else{
            res.emplace_back(std::get<0>(tmp),std::get<1>(tmp));
        }
    }

    if(res.empty()) res.emplace_back(0,0);

    return res;
}

void r_boss::build_overlap_tree(cache_config &config, size_t K, size_t min_K) {

    //LOG(INFO) <<"Building the overlap tree";
    virtual_kmer_tree::node_type tmp_node;
    std::vector<virtual_kmer_tree::node_type> tmp_children;

    virtual_kmer_tree vst(config, K);
    std::stack<virtual_kmer_tree::node_type> stack;

    stack.push(std::make_tuple(0, vst.n_leaves()-1, 0));
    sdsl::bit_vector par;

    std::vector<size_t>o_p(vst.n_leaves(),0);
    std::vector<size_t>c_p(vst.n_leaves(),0);

    while(!stack.empty()){
        tmp_node = stack.top();
        stack.pop();
        tmp_children = vst.get_children(tmp_node);
        for (size_t i = tmp_children.size(); i-- > 0;) {
            stack.push(tmp_children[i]);
        }

        if(get<2>(tmp_node)>=min_K && (vst.is_leaf(tmp_node) || vst.has_child_with_dollar(tmp_node))){
            //std::cout<<std::get<0>(tmp_node)<<" "<<std::get<1>(tmp_node)<<" "<<std::get<2>(tmp_node)<<std::endl;
            o_p[std::get<0>(tmp_node)]++;
            c_p[std::get<1>(tmp_node)]++;
        }
    }

    par.resize(4*vst.n_leaves());
    par[0] = true;
    size_t par_pos=1;
    for(size_t i=0;i<vst.n_leaves();i++){
        for(size_t j=0;j<o_p[i];j++){
            par[par_pos] = true;
            par_pos++;
        }
        for(size_t j=0;j<c_p[i];j++){
            par[par_pos] = false;
            par_pos++;
        }
    }
    par[par_pos] = false;
    par_pos++;
    par.resize(par_pos);

    //for (auto &&i : par) {
    //    if(i){
    //        std::cout<<"(";
    //    }else{
    //        std::cout<<")";
    //    }
    //}
    overlap_tree_bp ott(par);
    m_ovp_tree.swap(ott);

    //TODO just testing
    /*
    int_vector_buffer<> diff(cache_file_name("diff", config), std::ios::out);
    int_vector_buffer<> delta(cache_file_name("delta", config), std::ios::out);
    size_t tmp_ovp, n_runs=0, equals=0;
    size_t curr_head=0, max=0;
    bool is_head=true;

    for(size_t i=0;i<m_ovp_tree.get_n_leaves();i++){

        tmp_ovp=m_ovp_tree.overlap_leaf(i);

        if(tmp_ovp==0){
            //diff[i] =i;
            diff[i] = 0;
        }else{
            //diff[i] =tmp_ovp;
            diff[i] = (i-tmp_ovp);
        }

        if(diff[i]>max){
           max = diff[i];
        }

        if(i<1000){
           std::cout<<diff[i]<<std::endl;
        }

        if(i>0 && diff[i]!=diff[i-1]){
            n_runs++;
        }else{
            if(is_head){
                is_head=false;
                delta[i-1] = diff[i] - curr_head;
            }else{
                delta[i-1] = diff[i] - diff[i-1];
            }
            if(delta[i-1]>max){
               max = delta[i-1];
            }
        }
    }*/
/*
    std::vector<size_t> delta_freqs(max+1,0);
    for(size_t i=0;i<diff.size();i++){
        delta_freqs[diff[i]]++;
    }
    for(size_t i=0;i<delta_freqs.size();i++){
        std::cout<<i<<" "<<delta_freqs[i]<<std::endl;
    }
    size_t n_bits=0;
    for(size_t i=0;i<delta_freqs.size();i++){
        n_bits += (floor(log2(i+1))+1)*delta_freqs[i];
    }

    diff.close(false);
    sdsl::wt_huff<rrr_vector<63>> wt;
    sdsl::construct(wt, cache_file_name("diff", config));

    std::cout<<size_in_mega_bytes(wt)<<" "<<size_in_mega_bytes(m_ovp_tree)<<" "<<(double(n_bits)/8)/(1024*1024)<<std::endl;
    */
    /*bit_vector bv;
    bv.resize(acc);
    util::assign(bv, int_vector<1>(acc, 1));
    acc=0;
    size_t n_ones=0;
    for(size_t i=0;i<m_ovp_tree.get_n_leaves();i++){
        for(size_t j=0;j<diff[i]-1;j++){
            bv[acc] = false;
            acc++;
        }
        acc++;
    }

    sdsl::rrr_vector<63> comp_bv(bv);
    std::cout<<sdsl::size_in_mega_bytes(m_ovp_tree)<<" "<<sdsl::size_in_mega_bytes(comp_bv)<<std::endl;
    */
}

std::pair<r_boss::size_type, bool> r_boss::dbg_indegree(r_boss::size_type v) const {
    uint8_t symbol = m_edge_bwt.pos2symbol(v);
    size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
    size_t start, end;
    bool has_dollars;

    if(v==0) return make_pair(0,true);

    start = m_edge_bwt.select(rank, symbol);

    //this fits better in the select operation
    if(rank+1<=(m_edge_bwt.m_runs_acc[symbol] - m_edge_bwt.m_runs_acc[symbol - 1])){
        end = m_edge_bwt.select(rank+1, symbol);
    }else{
        end = m_edge_bwt.size();
    }

    size_t dollars_s = m_edge_bwt.m_dollars_rs.rank(start);
    size_t dollars_e = m_edge_bwt.m_dollars_rs.rank(end);

    size_t inv_s = m_edge_bwt.m_in_marks_rs.rank(start-dollars_s);
    size_t inv_e = m_edge_bwt.m_in_marks_rs.rank(end-dollars_e);

    has_dollars = m_solid_nodes[m_node_marks_rs.rank(start)]==0;

    return std::make_pair(1+m_edge_bwt.m_f_symbols.rank(inv_e, symbol)-m_edge_bwt.m_f_symbols.rank(inv_s, symbol),
                          has_dollars);
}

r_boss::size_type r_boss::reverse_complement(r_boss::size_type v) {
    std::vector<uint8_t> kmer = node_label(v);
    std::vector<uint8_t> reverse_complement(m_k-1);
    if(v==0) return 0;

    for(size_t i=0,j=kmer.size()-1;i<kmer.size();i++,j--){
        reverse_complement[i] = dna_alphabet::comp2rev[kmer[j]];
    }

    //backward search return the range in the bwt not the kmer id!!
    std::vector<std::pair<size_t, size_t>> ranges = backward_search(reverse_complement, 0);
    if(ranges.empty()){
        return 0;
    }else {
        return ranges[0].first;
    }
}


r_boss::label_t r_boss::node_label(r_boss::size_type v) const {
    label_t kmer(m_k-1);
    for(size_t i=0;i<m_k-1;i++){
        if(v==0){
            kmer[i] = 1; //only dollars
        }else {
            kmer[i] = m_edge_bwt.pos2symbol(v);
        }
        size_t rank = v - m_edge_bwt.m_runs_acc[kmer[i]-1];
        v = node_marks_rs.rank(m_edge_bwt.select(rank, kmer[i]));
    }
    return kmer;
}

r_boss::label_t r_boss::node_llabel(r_boss::size_type v) const {
    uint8_t symbol;
    assert(m_solid_nodes[v]==0);
    std::vector<uint8_t> llabel;

    while(v!=0){
        symbol = m_edge_bwt.pos2symbol(v);
        llabel.push_back(symbol);
        size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
        v = node_marks_rs.rank(m_edge_bwt.select(rank, symbol));
    }
    return llabel;
}

string r_boss::node2string(r_boss::size_type v, bool rev) const {
    std::vector<uint8_t> kmer = node_label(v);
    std::string kmer_seq;

    for (unsigned char l : kmer) {
        kmer_seq.push_back(dna_alphabet::comp2char[l]);
    }

    if(rev) std::reverse(kmer_seq.begin(), kmer_seq.end());

    return kmer_seq;
}

vector<r_boss::size_type> r_boss::f_overlaps(r_boss::size_type v) {

    assert(m_solid_nodes[v]);

    std::vector<size_type> f_overlaps;
    size_type nc = next_contained(v);
    if(nc == 0) return f_overlaps;

    label_t m_suffix(m_m);
    label_t greatest_linker = node_llabel(nc);

    //compute the reverse complement of the m_suffix
    size_t suff_pos = m_m-1;
    for (size_t i = 0; i <m_m;i++){
        m_suffix[suff_pos] = dna_alphabet::comp2rev[greatest_linker[i]];
        suff_pos--;
    }

    //backward search of the reverse complement of the m-suffix
    std::vector<std::pair<size_t, size_t>> bs = backward_search(m_suffix, 0);

    std::vector<uint8_t> symbols(7,0);
    std::vector<uint64_t> rank_i(7,0);
    std::vector<uint64_t> rank_j(7,0);
    size_t e_start, e_end, k_start, k_end, n_symbols;

    k_start = bs[0].first;
    k_end = bs[0].second;

    for (size_t i = m_m; i <greatest_linker.size(); i++) {

        for(size_t j=k_start;j<=k_end;j++){
            if(has_dollar_edges(get_edges(j))){
                //std::cout<<node2string(reverse_complement(j), true)<<std::endl;
            }
        }

        e_start = m_node_marks_ss.select(k_start) + 1;
        e_end= m_node_marks_ss.select(k_end+1);

        n_symbols = m_edge_bwt.range_symbols(e_start,
                                             e_end,
                                             symbols, rank_i, rank_j);
        bool found=false;

        for (size_t k = 0; k < n_symbols; k++) {
            if(symbols[k]==dna_alphabet::comp2rev[greatest_linker[i]]){
                found = true;
                k_start = m_edge_bwt.runs_acc[symbols[k] - 1] + rank_i[k];
                k_end = m_edge_bwt.runs_acc[symbols[k] - 1] + rank_j[k];

            }
        }

        if(!found) break;
    }

    //counting for the last one
    for(size_t j=k_start;j<=k_end;j++){
        if(has_dollar_edges(get_edges(j))){
            //std::cout<<node2string(reverse_complement(j), true)<<std::endl;
        }
    }
    return f_overlaps;
}

r_boss::size_type r_boss::dbg_incomming(r_boss::size_type v, uint8_t t) const {
    //TODO if the first is dollar, then move to the next as with outgoing
    assert(t > 0);
    if (v == 0) return 0;
    uint8_t symbol = m_edge_bwt.pos2symbol(v);
    size_t rank = v - m_edge_bwt.m_runs_acc[symbol - 1];
    size_t edge_pos = m_edge_bwt.select(rank, symbol);

    if (t == 1) {
        return m_node_marks_rs.rank(edge_pos);
    } else {
        size_t next_edge_pos = m_edge_bwt.select(rank + 1, symbol);

        size_t dollars_s = m_edge_bwt.m_dollars_rs.rank(edge_pos);
        size_t dollars_e = m_edge_bwt.m_dollars_rs.rank(next_edge_pos);

        size_t inv_s = m_edge_bwt.m_in_marks_rs.rank(edge_pos - dollars_s);
        size_t inv_e = m_edge_bwt.m_in_marks_rs.rank(next_edge_pos - dollars_e);

        size_t rank_i = m_edge_bwt.m_f_symbols.rank(inv_s, symbol);
        size_t rank_j = m_edge_bwt.m_f_symbols.rank(inv_e, symbol);

        assert((t - 1U) <= (rank_j - rank_i));

        size_t symbol_rank = m_edge_bwt.m_f_symbols.select(rank_i + t - 1, symbol) + 1;
        size_t tmp = edge_pos - dollars_s;
        while (inv_s != symbol_rank) {
            if (m_edge_bwt.m_in_marks[tmp]) {
                inv_s++;
            }
            tmp++;
        }

        return m_node_marks_rs.rank(tmp - 1 + dollars_s);
        //return m_node_marks_rs.rank(m_edge_bwt.m_in_marks_ss.select(n_zeroes)+tmp_2+dollars_s);
    }
}

/*vector<r_boss::edge_t> r_boss::foverlaps(r_boss::size_type v){

    size_type curr_kmer=v, tmp_kmer;
    size_t out_d;

    std::vector<uint8_t> curr_seq;
    std::vector<uint8_t> tmp_seq;
    std::stack<r_boss::edge_t> p_kmers;
    vector<r_boss::edge_t> outgoing_s_kmers;

    while((curr_kmer= next_contained(curr_kmer)) !=0){
        p_kmers.push({curr_kmer, std::vector<uint8_t>()});
    }

    while(!p_kmers.empty()){

        curr_kmer = p_kmers.top().out_node;
        curr_seq = p_kmers.top().label;
        p_kmers.pop();

        if(!solid_nodes[curr_kmer]){
            out_d = dbg_outdegree(curr_kmer).first;
            for(uint8_t i=1;i<=out_d;i++){
               tmp_kmer = dbg_forward(curr_kmer, i);
               tmp_seq = curr_seq;
               tmp_seq.push_back(edge_bwt.pos2symbol(tmp_kmer));
               p_kmers.push({tmp_kmer, tmp_seq});
            }
        }else{
            outgoing_s_kmers.push_back({curr_kmer, curr_seq});
        }
    }
    return outgoing_s_kmers;
}

bool r_boss::is_ovp_uniq(size_type v, std::vector<uint8_t> &label, vector<size_type> &tran_nodes,
                            size_type& l, size_type& k) {

    size_type curr_kmer=v, out_node;
    std::vector<size_type> p_kmers, p_kmers_ed;
    r_boss::range_t tmp_edges;

    l=0;
    k=0;

    while((curr_kmer= next_contained(curr_kmer)) !=0){
        tmp_edges = get_edges(curr_kmer);

        if((tmp_edges.second-tmp_edges.first+1)>1){//outdegree >1
            tran_nodes[0] = v;
            k=1;
            return false;
        }

        p_kmers.push_back(curr_kmer);
        p_kmers_ed.push_back(tmp_edges.first);
    }

    if(p_kmers.empty()){ //empty overlap return true
       tran_nodes[0] = 0;
       k=1;
       return true;
    }

    if(p_kmers.size()>1 && !m_edge_bwt.same_symbol(p_kmers_ed)){
        tran_nodes[0] = v;
        k=1;
        return false;
    }

    while(!m_solid_nodes[p_kmers.back()]){

        std::map<size_t, bool> zero_out;

        for (size_t i = 0; i < p_kmers.size(); i++) {

            out_node = dbg_forward(p_kmers[i], 1);
            tmp_edges = get_edges(out_node);

            p_kmers[i] = out_node;
            p_kmers_ed[i] = tmp_edges.first + edge_bwt.m_dollars[tmp_edges.first];

            if(tmp_edges.first==tmp_edges.second && edge_bwt.m_dollars[tmp_edges.first]){
                zero_out[i]=true;
            }

            //store transitive solid nodes
            if(m_solid_nodes[p_kmers[i]]){
                if(tran_nodes.empty() || tran_nodes[k-1]!=p_kmers[i]){
                    tran_nodes[k] = p_kmers[i];
                    k++;
                }
            }

            if ((tmp_edges.second - tmp_edges.first + 1) > 1 && !edge_bwt.m_dollars[tmp_edges.first]) {//outdegree > 1
                label[l] = m_edge_bwt.pos2symbol(out_node);
                l++;

                if(k==0){
                    if(m_solid_nodes[p_kmers[0]]){
                        tran_nodes[0] = p_kmers[0];
                    }else{
                        tran_nodes[0] = v;
                    }
                    k = 1;
                }
                return false;
            }
        }

        //remove the kmer with outdegree ==0
        std::vector<size_type> tmp1;
        std::vector<size_type> tmp2;
        for(size_t i=0;i<p_kmers.size();i++){
            if(zero_out.count(i)==0){
                tmp1.push_back(p_kmers[i]);
                tmp2.push_back(p_kmers_ed[i]);
            }
        }
        p_kmers = tmp1;
        p_kmers_ed = tmp2;

        label[l] = m_edge_bwt.pos2symbol(p_kmers[0]);
        l++;

        if(p_kmers.size()>=2 && !m_edge_bwt.same_symbol(p_kmers_ed)){
            if(k==0){
                tran_nodes[0] = v;
                k = 1;
            }
            return false;
        }
    }

    return true;
}

bool r_boss::unique_out(size_t v, std::vector<uint8_t> &label, std::vector<size_type> &trans_nodes,
                           size_type& l, size_type& k) {

    bool uniq_o, has_dollars;
    size_type eff_edges, out_node, prev_out_node, last_safe_node=0;

    range_t edges = get_edges(v);
    has_dollars = has_dollar_edges(edges);
    eff_edges = edges.second-edges.first+1 - has_dollars;
    r_boss::degree_t out_d;
    l=0;
    k=0;
    //more than dbg edges (other than $)
    if(eff_edges>=2){
        trans_nodes[0] = v;
        k=1;
        return false;
    }

    //exactly one dbg edge (other than $)
    if(eff_edges==1 && !has_dollars){
        out_node = dbg_forward(edges, 1);
        label[0]  = edge_bwt.pos2symbol(out_node);
        trans_nodes[0]= out_node;
        l=1;
        k=1;
        return true;

    }else if(eff_edges<=1 && has_dollars){ //one $ dbg edge with 0 or more non-$ dbg edges

        uniq_o = is_ovp_uniq(v, label, trans_nodes, l, k);

        out_node = v;
        prev_out_node = v;
        out_d = {edges.second-edges.first+1, has_dollars};

        //there are overlaps, but there is no outgoing edge for v
        if(uniq_o && l!=0 && eff_edges==0) return uniq_o;

        //there are no overlaps for v, but there is an outgoing edge
        if(uniq_o && l==0 && k==1 && eff_edges==1){
            trans_nodes[0] = dbg_forward(edges, 1);
            label[0] = edge_bwt.pos2symbol(trans_nodes[0]);
            l=1;
            return uniq_o;
        }

        //compare the label with the path of the node
        if(eff_edges>0) {
            size_t p = 0, i=0;
            while(i < l) {

                out_node = dbg_forward(out_node, 1);
                out_d = dbg_outdegree(out_node);

                if (out_node == trans_nodes[p]) p++;

                //the label of v is not completely safe
                if(edge_bwt.pos2symbol(out_node) != label[i]){
                    l = i + 1;
                    last_safe_node = prev_out_node;
                    uniq_o = false;
                }

                //the label of v is not completely safe
                if((out_d.first - out_d.second) > 1) {
                    l = i + 1;
                    last_safe_node = out_node;
                    uniq_o = false;
                }

                //TODO instead of shorten it, it might be better to return the complete label (when the overlap is not uniq)
                //label spelled by v, ended before size(label)
                if ((out_d.first - out_d.second) == 0) {
                    l = i + 1;
                    last_safe_node = out_node;
                }

                prev_out_node = out_node;
                i++;
            }

            //there is no unique overlap, but there is some characters that can be useful
            if(l!=0 && i!=l){
                if (p == 0 || trans_nodes[p - 1] != last_safe_node) {
                    trans_nodes[p] = last_safe_node;
                    p++;
                }
                k = p;
            }
        }
        return uniq_o;
    }
    return false;
}

bool r_boss::unique_in(size_t v, std::vector<uint8_t> &label, std::vector<size_type> &trans_nodes,
                          size_type& l, size_type& k) {

    //TODO get the reverse complement of transition nodes
    return unique_out(reverse_complement(v), label, trans_nodes, l, k);
}

vector<r_boss::size_type> r_boss::outgoings(r_boss::size_type v) {

    size_type curr_kmer=v, tmp_kmer;
    degree_t out_d;
    size_t l=0;
    std::stack<pair<size_type, size_type>> p_kmers;
    stack<size_type> tmp_stack;
    std::pair<size_type, size_type> kmer_info;

    std::vector<size_type> final_outs;
    std::map<size_type, bool> outgoing_map;

    p_kmers.push({v,0});

    while((curr_kmer= next_contained(curr_kmer)) !=0){
        p_kmers.push({curr_kmer,0});
    }

    //traverse the first kmer to know how far we have to traverse the graph
    tmp_stack.push(p_kmers.top().first);
    p_kmers.pop();
    while(!tmp_stack.empty()){

        curr_kmer = tmp_stack.top();
        tmp_stack.pop();

        if(!solid_nodes[curr_kmer]){
            out_d = dbg_outdegree(curr_kmer);
            for(uint8_t i=1;i<=(out_d.first-out_d.second);i++){
                tmp_kmer = dbg_outgoing(curr_kmer, i);
                tmp_stack.push(tmp_kmer);
            }
        }else{
            outgoing_map[curr_kmer]=true;
        }
        l++;
    }
    l--;

    while(!p_kmers.empty()){

        kmer_info = p_kmers.top();
        p_kmers.pop();

        if(kmer_info.second<l){
            out_d = dbg_outdegree(kmer_info.first);
            for(uint8_t i=1;i<=(out_d.first-out_d.second);i++){
                tmp_kmer = dbg_outgoing(kmer_info.first, i);
                p_kmers.push({tmp_kmer, kmer_info.second+1});
            }
        }else{
            outgoing_map[kmer_info.first] = true;
        }
    }

    for ( const auto &outs : outgoing_map ) {
        final_outs.push_back(outs.first);
    }
    return final_outs;
}

}*/
