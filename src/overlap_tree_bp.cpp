//
// Created by Diego Diaz on 3/26/18.
//

#include <overlap_tree_bp.hpp>

#include "overlap_tree_bp.hpp"

using namespace sdsl;
using namespace std;

overlap_tree_bp::overlap_tree_bp() {
    m_n_leaves = 0;
}

overlap_tree_bp::overlap_tree_bp(const overlap_tree_bp &ts) {
    m_n_leaves = 0;
    copy(ts);
}

overlap_tree_bp::overlap_tree_bp(overlap_tree_bp::t_bit_vector &shape) {
    //create tree shape structures
    m_shape.swap(shape);
    util::assign(m_bp_supp, t_bp_support(&m_shape));
    util::init_support(m_bp_rank_10, &m_shape);
    util::init_support(m_bp_select_10, &m_shape);
    m_n_leaves = m_bp_rank_10(m_shape.size());
}

overlap_tree_bp::size_type overlap_tree_bp::get_tot_nodes() const {
    return m_bp_supp.rank(m_shape.size()-1);
}

overlap_tree_bp::size_type overlap_tree_bp::get_n_leaves() const {
    return m_n_leaves;
}

overlap_tree_bp::size_type overlap_tree_bp::get_int_nodes() const {
    return get_tot_nodes() - m_n_leaves;
}

void overlap_tree_bp::print_shape() const {
    char tmp;
    for (unsigned long i : m_shape) {
        tmp = i ? '(' : ')';
        cout<<tmp<<" ";
    }
    cout<<""<<endl;
}

overlap_tree_bp::node_type overlap_tree_bp::range_to_id(overlap_tree_bp::range_type& range) const {

    if(!is_range_valid(range)){
        throw range_error("tree range is not valid");
    }
    // least common ancestor
    return lca(select_leaf(get<0>(range)+1),
               select_leaf(get<1>(range)+1));
}

overlap_tree_bp::range_type overlap_tree_bp::id_to_range(overlap_tree_bp::node_type id) const {
    // check if ID is a valid range
    if(!is_id_valid(id)){
        throw invalid_argument("node ID is not valid");
    }
    return make_pair(lb(id), rb(id));
}

//child range is zero based
overlap_tree_bp::range_type overlap_tree_bp::get_parent_range(overlap_tree_bp::range_type& child_range) const {

    if(!is_range_valid(child_range)){
        throw range_error("tree range is not valid");
    }

    // get the ID of range
    // (I could call range_to_id but I didn't want to do double check on the range!)
    node_type child_id = lca(select_leaf(get<0>(child_range)+1),
                             select_leaf(get<1>(child_range)+1));

    if(child_id==root()){
        throw invalid_argument("asking for the parent of the root");
    }
    // return the leaves of the parenthesis enclosing the input node
    return id_to_range(m_bp_supp.enclose(child_id));
}

overlap_tree_bp::range_type overlap_tree_bp::contains(overlap_tree_bp::range_type& range)const{
    node_type lca_node = lca(select_leaf(get<0>(range)+1),
                            select_leaf(get<1>(range)+1));

    // get the range of least common ancestor
    return id_to_range(lca_node);
}

bool overlap_tree_bp::is_leaf(overlap_tree_bp::node_type v) const {
    assert(m_shape[v]==1);  // assert that v is a valid node of the suffix tree
    // if there is a closing parenthesis at position v+1, the node is a leaf
    return !m_shape[v+1];
}

overlap_tree_bp::size_type overlap_tree_bp::degree(overlap_tree_bp::range_type& range) const {
    if(!is_range_valid(range)){
        throw range_error("tree range is not valid");
    }

    node_type v = range_to_id(range);
    size_type res = 0;
    v = v+1;
    while (m_shape[v]) { // found open parentheses
        ++res;
        v = m_bp_supp.find_close(v)+1;
    }
    return res;
}

overlap_tree_bp::node_type overlap_tree_bp::root() const {
    return 0;
}

vector<overlap_tree_bp::range_type> overlap_tree_bp::children(overlap_tree_bp::range_type& v) const {
    if(!is_range_valid(v)){
        throw range_error("tree range is not valid");
    }

    vector<range_type> children;
    size_type parent_id = range_to_id(v);
    size_type tmp_child = parent_id+1;
    while(true){
        if(!m_shape[tmp_child]){
            break;
        }else {
            children.push_back(id_to_range(tmp_child));
            tmp_child = m_bp_supp.find_close(tmp_child) + 1;
        }
    }
    return children;
}

overlap_tree_bp &overlap_tree_bp::operator=(const overlap_tree_bp &ts_new) {
    // check for self-assignment
    if(&ts_new == this)
    {
        return *this;
    }else{
        copy(ts_new);
    }
    return *this;
}

void overlap_tree_bp::swap(overlap_tree_bp &ts_new) {
    m_shape.swap(ts_new.m_shape);
    util::swap_support(m_bp_supp, ts_new.m_bp_supp, &m_shape, &(ts_new.m_shape));
    util::swap_support(m_bp_rank_10, ts_new.m_bp_rank_10, &m_shape, &(ts_new.m_shape));
    util::swap_support(m_bp_select_10, ts_new.m_bp_select_10, &m_shape, &(ts_new.m_shape));
    m_n_leaves = ts_new.m_n_leaves;
}

overlap_tree_bp::size_type overlap_tree_bp::serialize(ostream &out, structure_tree_node *v, string name) const {
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    written_bytes += m_shape.serialize(out, child, "shape");
    written_bytes += m_bp_supp.serialize(out, child, "bp_supp");
    written_bytes += m_bp_rank_10.serialize(out, child, "bp_rank_10");
    written_bytes += m_bp_select_10.serialize(out, child, "bp_select_10");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void overlap_tree_bp::load(istream &in) {
    m_shape.load(in);
    m_bp_supp.load(in, &m_shape);
    m_bp_rank_10.load(in, &m_shape);
    m_bp_select_10.load(in, &m_shape);
    m_n_leaves = m_bp_rank_10(m_shape.size()-1);
}

DfsIterator overlap_tree_bp::begin() const {
    return {&m_shape, 2, get_tot_nodes(), &m_bp_supp};
}

DfsIterator overlap_tree_bp::end() const {
    return {&m_shape, 1, 1, &m_bp_supp};
}

overlap_tree_bp::node_type overlap_tree_bp::select_leaf(overlap_tree_bp::size_type i) const {
    assert(i > 0 and i <= m_n_leaves);
    // -1 as select(i) returns the position of the 0 of pattern 10
    return m_bp_select_10.select(i)-1;
}

overlap_tree_bp::node_type overlap_tree_bp::leftmost_leaf(const overlap_tree_bp::node_type& v) const {
    return m_bp_select_10(m_bp_rank_10(v)+1)-1;
}

overlap_tree_bp::node_type overlap_tree_bp::rightmost_leaf(const overlap_tree_bp::node_type& v) const {
    size_type r = m_bp_supp.find_close(v);
    return m_bp_select_10(m_bp_rank_10(r+1))-1;
}

//zero-based values
overlap_tree_bp::size_type overlap_tree_bp::lb(const overlap_tree_bp::node_type& v) const {
    return m_bp_rank_10(v);
}

//also zero-based values
overlap_tree_bp::size_type overlap_tree_bp::rb(const overlap_tree_bp::node_type& v) const {
    size_type r = m_bp_supp.find_close(v);
    return m_bp_rank_10(r+1)-1;
}

overlap_tree_bp::node_type overlap_tree_bp::lca(overlap_tree_bp::node_type v, overlap_tree_bp::node_type w) const {
    assert(m_shape[v] == 1 and m_shape[w] == 1);
    if (v > w) {
        std::swap(v,w);
    } else if (v==w) {
        return v;
    }
    if (v == root())
        return root();
    return m_bp_supp.double_enclose(v, w);
}

overlap_tree_bp::node_type overlap_tree_bp::sibling(overlap_tree_bp::node_type& v) const {
    if (v==root())
        return root();
    node_type sib = m_bp_supp.find_close(v)+1;
    if (m_shape[sib])
        return sib;
    else
        return root();
}

bool overlap_tree_bp::is_range_valid(overlap_tree_bp::range_type& range) const {
    size_type sp = get<0>(range);
    size_type ep = get<1>(range);

    if(sp > ep){
        return false;
    }
    if( sp>=m_n_leaves || ep >= m_n_leaves){
        return false;
    }
    return true;
}

bool overlap_tree_bp::is_id_valid(overlap_tree_bp::node_type& v) const {
    if(v>=m_shape.size()){
        return false;
    }else if(!m_shape[v]){
        return false;
    }
    return true;
}

void overlap_tree_bp::copy(const overlap_tree_bp &ts) {
    m_n_leaves         = ts.m_n_leaves;
    m_shape            = ts.m_shape;
    m_bp_supp          = ts.m_bp_supp;
    m_bp_supp.set_vector(&m_shape);
    m_bp_rank_10       = ts.m_bp_rank_10;
    m_bp_rank_10.set_vector(&m_shape);
    m_bp_select_10     = ts.m_bp_select_10;
    m_bp_select_10.set_vector(&m_shape);
}

overlap_tree_bp::node_type overlap_tree_bp::contains(overlap_tree_bp::node_type v, overlap_tree_bp::node_type w) {
    if(!is_id_valid(v) || !is_id_valid(w)){
        throw std::invalid_argument("node ids are not valid");
    }
    // get the range of least common ancestor
    return lca(v, w);
}

overlap_tree_bp::size_type overlap_tree_bp::degree(overlap_tree_bp::node_type &v) const {
    if(!is_id_valid(v)){
        throw range_error("tree range is not valid");
    }

    size_type res = 0;
    v = v+1;
    while (m_shape[v]) { // found open parentheses
        ++res;
        v = m_bp_supp.find_close(v)+1;
    }
    return res;
}

vector<overlap_tree_bp::node_type> overlap_tree_bp::children(overlap_tree_bp::node_type &v) const {
    if(!is_id_valid(v)){
        throw range_error("tree range is not valid");
    }

    vector<node_type> children;
    size_type tmp_child = v+1;
    while(true){
        if(!m_shape[tmp_child]){
            break;
        }else {
            children.push_back(tmp_child);
            tmp_child = m_bp_supp.find_close(tmp_child) + 1;
        }
    }
    return children;
}

bool overlap_tree_bp::is_root(overlap_tree_bp::range_type &v) const {
    return v.first == 0 && v.second == (m_n_leaves - 1);
}

bool overlap_tree_bp::is_leaf(overlap_tree_bp::range_type &v) const {
    return v.first==v.second;
}

overlap_tree_bp::size_type overlap_tree_bp::overlap_leaf(overlap_tree_bp::size_type leaf) const {
    assert(leaf<n_leaves);
    if(leaf==0) return 0;
    range_type leaf_range = {leaf, leaf};
    node_type leaf_node = range_to_id(leaf_range);
    node_type least_sibling = m_bp_supp.enclose(leaf_node)+1;
    assert(m_shape[least_sibling]); //least brother must exists
    if(least_sibling==leaf_node){
        least_sibling = m_bp_supp.enclose(least_sibling-1)+1;
    }

    return lb(least_sibling);
}

overlap_tree_bp::children_iter_t overlap_tree_bp::children_iter(overlap_tree_bp::size_type &v) const {
    return children_iterator(&m_shape, &m_bp_supp, v);
}

overlap_tree_bp::size_type
overlap_tree_bp::select_child(overlap_tree_bp::size_type v, overlap_tree_bp::size_type child_rank) const {

    if (is_leaf(v))  // if v is a leave, v has no child
        return root();
    size_type res = v+1;
    while (child_rank > 1) {
        res = m_bp_supp.find_close(res)+1;
        if (!m_shape[res]) {// closing parenthesis: there exists no next child
            return root();
        }
        --child_rank;
    }
    return res;
}

