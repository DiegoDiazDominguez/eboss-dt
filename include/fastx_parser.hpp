//
// Created by Diego Diaz on 3/19/18.
//

#ifndef RBOSS_FASTX_PARSER_HPP
#define RBOSS_FASTX_PARSER_HPP

extern "C"{
#include "kseq.h"
#include <zlib.h>
#include <cstdio>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
};
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sdsl/sd_vector.hpp>
#include "dna_alphabet.hpp"


class FastXParser {
    KSEQ_INIT(gzFile, gzread);
public:
    static int preproc_reads(std::string &input_file, sdsl::cache_config& config, size_t kmer_size);
};


#endif //RBOSS_FASTX_PARSER_HPP
