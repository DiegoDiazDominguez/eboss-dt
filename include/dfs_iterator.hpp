//
// Created by Diego Diaz on 3/25/18.
//

#ifndef OMNITIGSUNIBOSS_DFS_ITERATOR_HPP
#define OMNITIGSUNIBOSS_DFS_ITERATOR_HPP

#include <iterator>
#include <vector>
#include <sdsl/bp_support_sada.hpp>
#include <sdsl/rank_support_v5.hpp>
#include <sdsl/select_support_mcl.hpp>

class DfsIterator{

    typedef sdsl::bp_support_sada<> t_bp_support;
    typedef sdsl::rank_support_v5<10,2> t_rank_10;

public:
    // Iterator traits, previously from std::iterator.
    using value_type = unsigned long long int;
    using difference_type = std::ptrdiff_t;
    using pointer = value_type*;
    using reference = value_type&;
    using iterator_category = std::bidirectional_iterator_tag;

    const value_type & curr_rank;

    // Default constructible.
    DfsIterator() = default;
    DfsIterator(const sdsl::bit_vector *shape, value_type starter, value_type final, const t_bp_support *bp_supp);

    // Dereferencable.
    value_type operator*() const;

    // Pre- and post-incrementable.
    DfsIterator& operator++();
    const DfsIterator operator++(int);

    // Pre- and post-decrementable.
    DfsIterator& operator--();
    const DfsIterator operator--(int);

    // Equality / inequality.
    bool operator==(const DfsIterator& rhs);
    bool operator!=(const DfsIterator& rhs);

private:
    const sdsl::bit_vector* m_shape;
    const t_bp_support* m_bp_supp;

    // The node this iterator references.
    value_type m_curr_rank;
    value_type m_final_rank;
};

#endif //OMNITIGSUNIBOSS_DFS_ITERATOR_HPP
