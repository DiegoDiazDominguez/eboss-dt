//
// Created by Diego Diaz on 3/19/18.
//
#ifndef RBOSS_R_BOSS_HPP
#define RBOSS_R_BOSS_HPP

#include <iostream>
#include <fstream>
#include <bitset>

#include <limits>
#include <sdsl/sdsl_concepts.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/construct.hpp>
#include <sdsl/construct_lcp.hpp>
#include <sdsl/construct_sa.hpp>
#include <sdsl/construct_bwt.hpp>
#include <sdsl/wt_algorithm.hpp>
#include <sdsl/sd_vector.hpp>

#include "rle_ebwt.hpp"
#include "overlap_tree_bp.hpp"
#include "virtual_kmer_tree.hpp"
#include "dna_alphabet.hpp"
#include "fastx_parser.hpp"

class r_boss{

private:
    typedef rrr_vector<63>                            t_comp_bv;
    typedef typename rrr_vector<63>::rank_0_type      t_rank_0;
    typedef typename rrr_vector<63>::select_0_type    t_select_0;
    typedef typename rrr_vector<63>::select_1_type    t_select_1;

    size_t                                            m_k;
    size_t                                            m_m;
    size_t                                            m_n_solid_nodes=0; //# of kmers without dollar symbols

    rle_ebwt                                          m_edge_bwt;
    overlap_tree_bp                                   m_ovp_tree;
    t_comp_bv                                         m_node_marks;
    t_rank_0                                          m_node_marks_rs;
    t_select_0                                        m_node_marks_ss;
    t_comp_bv                                         m_solid_nodes;
    t_select_1                                        m_solid_nodes_ss;

public:
    typedef uint64_t                                  size_type;
    typedef std::pair<size_type, size_type>           range_t;
    typedef std::pair<size_type, bool>                degree_t;
    typedef std::vector<uint8_t>                      label_t;

    const size_t &k                     =             m_k;
    const size_t &m                     =             m_m;
    const size_t &n_solid_nodes         =             m_n_solid_nodes;

    const t_comp_bv& node_marks         =             m_node_marks;
    const rle_ebwt& edge_bwt            =             m_edge_bwt;
    const overlap_tree_bp& ovp_tree     =             m_ovp_tree;
    const t_comp_bv& solid_nodes        =             m_solid_nodes;

    const t_rank_0& node_marks_rs       =             m_node_marks_rs;
    const t_select_0& node_marks_ss     =             m_node_marks_ss;
    const t_select_1& solid_nodes_ss    =             m_solid_nodes_ss;

public:
    //constructors
    r_boss();
    r_boss(std::string& input_file, cache_config& config, const size_t & K, const size_t & min_str_depth);

    //dBG navigational functions
    size_type dbg_outgoing(range_t v, uint8_t t) const;
    size_type dbg_outgoing(size_type v, uint8_t t) const;
    size_type dbg_incomming(size_type v, uint8_t t) const;
    degree_t dbg_outdegree(size_type v) const;
    degree_t dbg_indegree(size_type v) const;

    //overlap graph functions
    std::vector<size_type> f_overlaps(size_type v);
    size_type next_contained(size_type v) const;

    //other types of functions
    std::vector<range_t> backward_search(label_t &query, uint8_t k);
    size_type reverse_complement(size_type v);
    label_t node_label(size_type v) const;
    label_t node_llabel(size_type v) const;
    bool has_dollar_edges(range_t range) const;
    range_t get_edges(size_type v) const;
    std::vector<size_type> build_l(size_type v) const;
    std::string node2string(size_type v, bool rev) const;

    //statistic functions
    inline size_type num_of_edges() const {return m_edge_bwt.size();};
    size_type num_of_nodes() const {return m_ovp_tree.n_leaves;};
    size_type num_of_dollar_edges() const{ return m_edge_bwt.m_dollars_rs(m_edge_bwt.size());}
    size_type num_of_marked_edges() const{ return m_edge_bwt.m_f_symbols.size();}

    //storage functions
    size_type serialize(std::ostream& out, structure_tree_node* v, std::string name)const;
    void load(std::istream&);

    //legacy functions (I used them at some point in the past)
    /*
    std::vector<size_type> outgoings(size_type v);
    bool is_ovp_uniq(size_type v, std::vector<uint8_t> &label, std::vector<size_type> &tran_nodes,
                     size_type& l, size_type& k);
    bool unique_out(size_t v, std::vector<uint8_t> &label,
                    std::vector<size_type> &trans_nodes,
                    size_type& l, size_type& k);
    bool unique_in(size_t v, std::vector<uint8_t> &label,
                    std::vector<size_type> &trans_nodes,
                    size_type& l, size_type& k);*/
    //

private:
    void build_sa_bwt_lcp(cache_config &config);
    void build_edgebwt(cache_config &config, size_t K);
    void build_klcp(cache_config &config, size_t K);
    void build_overlap_tree(cache_config &config, size_t K, size_t min_K);
};

inline r_boss::size_type r_boss::dbg_outgoing(range_t v, uint8_t t) const {
    if(has_dollar_edges(v)) t++;
    assert(t>0 && t<=(v.second-v.first+1));
    return m_edge_bwt.LFmapping(std::get<0>(v)+ t-1);
};//t is the rank inside the range [1..\sigma]

inline r_boss::size_type r_boss::dbg_outgoing(size_type v, uint8_t t) const {
    range_t edges = get_edges(v);
    if(has_dollar_edges(edges)) t++;
    assert(t>0 && t<=(edges.second-edges.first+1));
    return m_edge_bwt.LFmapping(std::get<0>(edges)+ t-1);
};//t is the rank inside the range [1..\sigma]

inline r_boss::degree_t r_boss::dbg_outdegree(r_boss::size_type v) const {
    range_t range;
    bool has_dollar;
    range = get_edges(v);
    has_dollar = has_dollar_edges(range);
    return std::make_pair(range.second-range.first+1, has_dollar);
};

inline r_boss::size_type r_boss::next_contained(r_boss::size_type v) const {
    return ovp_tree.overlap_leaf(v);
};

inline bool r_boss::has_dollar_edges(r_boss::range_t range) const{
    return (m_edge_bwt.m_dollars_rs.rank(range.second+1)-m_edge_bwt.m_dollars_rs.rank(range.first))!=0;
}

inline r_boss::range_t r_boss::get_edges(r_boss::size_type v) const {
    size_t start, end;
    if(v==0){
        start=0;
    }else {
        start = m_node_marks_ss.select(v) + 1;
    }
    end = m_node_marks_ss.select(v+1);
    return std::make_pair(start, end);
}

inline std::vector<r_boss::size_type> r_boss::build_l(r_boss::size_type v) const{
    std::vector<r_boss::size_type> l;
    while((v= next_contained(v)) !=0) l.push_back(v);
    return l;
}

#endif //RBOSS_R_BOSS
