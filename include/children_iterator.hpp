//
// Created by diediaz on 13-12-18.
//

#ifndef OMNITIGSHOBOSS_CHILDRENITERATOR_HPP
#define OMNITIGSHOBOSS_CHILDRENITERATOR_HPP

#include <sdsl/bp_support.hpp>
#include <sdsl/csa_sada.hpp>

class children_iterator {

private:
    typedef sdsl::bp_support_sada<> t_bp_support;
    typedef typename sdsl::csa_sada<>::size_type    size_type;
    size_type m_curr_child;
    bool m_has_children;
    const sdsl::bit_vector* m_tree_shape;
    const t_bp_support* m_bp_supp;
public:
    children_iterator();
    children_iterator(const sdsl::bit_vector *tree_shape, const t_bp_support* bp_supp, size_type father_node);
    size_type operator*()const;
    const children_iterator operator--(int);
    children_iterator& operator=(const children_iterator& other);
    bool hasChildren();

};


#endif //OMNITIGSHOBOSS_CHILDRENITERATOR_HPP
