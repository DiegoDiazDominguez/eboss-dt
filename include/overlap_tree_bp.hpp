//
// Created by Diego Diaz on 3/26/18.
//

#ifndef RBOSS_OVERLAP_TREE_BP_HPP
#define RBOSS_OVERLAP_TREE_BP_HPP

#include <sdsl/rrr_vector.hpp>
#include <sdsl/bp_support.hpp>
#include <sdsl/wt_int.hpp>
#include <sdsl/enc_vector.hpp>
#include <sdsl/construct.hpp>
#include <sdsl/csa_sada.hpp>
#include <sdsl/sdsl_concepts.hpp>
#include "dfs_iterator.hpp"
#include "children_iterator.hpp"

class overlap_tree_bp {

private:
    friend class ChildrenIterator;
    typedef sdsl::bp_support_sada<> t_bp_support;
    typedef sdsl::rank_support_v5<10,2> t_rank_10;
    typedef sdsl::select_support_mcl<10,2> t_select_10;
    typedef sdsl::bit_vector t_bit_vector;

public:
    typedef typename sdsl::csa_sada<>::size_type    size_type;
    typedef std::pair<size_type, size_type>         range_type;
    typedef size_type                               node_type;
    typedef DfsIterator                             iterator;
    typedef children_iterator                       children_iter_t;

private:
    size_type                                       m_n_leaves{};
    t_bp_support                                    m_bp_supp;
    t_rank_10                                       m_bp_rank_10;
    t_select_10                                     m_bp_select_10;
    t_bit_vector                                    m_shape;

public:
    const size_type& n_leaves =                     m_n_leaves;
    const t_bit_vector& shape =                     m_shape;
    const t_bp_support& bp_supp =                   m_bp_supp;
    const t_rank_10& bp_rank_10 =                   m_bp_rank_10;
    const t_select_10& bp_select_10 =               m_bp_select_10;

private:
    node_type select_leaf(size_type i) const;
    node_type leftmost_leaf(const node_type& v)const;
    node_type rightmost_leaf(const node_type& v)const;
    size_type lb(const node_type& v)const;
    size_type rb(const node_type& v)const;
    node_type lca(node_type v, node_type w)const;
    node_type sibling(node_type& v)const;
    bool is_range_valid(range_type& range)const;
    bool is_id_valid(node_type& v)const;
    void copy(const overlap_tree_bp& ts);

public:
    overlap_tree_bp();
    overlap_tree_bp(const overlap_tree_bp& ts);
    explicit overlap_tree_bp(t_bit_vector& shape);

    size_type get_tot_nodes()const;
    size_type get_n_leaves()const;
    size_type get_int_nodes()const;
    void print_shape()const;

    node_type range_to_id(range_type& range)const;
    range_type id_to_range(node_type id)const;
    range_type get_parent_range(range_type& child_range)const;
    range_type contains(range_type& range)const;
    node_type contains(node_type v, node_type w);
    size_type overlap_leaf(size_type leaf) const;

    bool is_leaf(node_type v)const;
    bool is_leaf(range_type& v)const;
    size_type degree(range_type& range)const;
    size_type degree(node_type& v)const;
    size_type select_child(size_type v, size_type child_rank)const;
    std::vector<range_type> children(range_type& v)const;
    children_iter_t children_iter(size_type &v)const;
    std::vector<node_type> children(node_type& v)const;
    node_type root()const;
    bool is_root(range_type& v)const;

    overlap_tree_bp& operator=(const overlap_tree_bp& ts_new);
    void swap(overlap_tree_bp& ts_new);

    size_type serialize(std::ostream &out, sdsl::structure_tree_node *v, std::string name) const;
    void load(std::istream& in);

    DfsIterator begin() const;
    DfsIterator end() const;

};


#endif //RBOSS_KMER_TREE_SHAPE_HPP
